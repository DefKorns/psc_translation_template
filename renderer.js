// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
let app = require('electron').remote;
let dialog = app.dialog;
let fs = require('fs');

document.getElementById('createButton').onclick =()=> {
	dialog.showSaveDialog((fileName) => {
		if(fileName === undefined) {
			alert("You didn't save the file");
			return;
		}
		let content = document.getElementById('content').value;
		fs.writeFile(fileName, content, (err) => {
			if (err) {
				console.log(err);
			}
			alert("The file has been successfully saved");
		});
	});
};

document.getElementById('openButton').onclick = () => {
	dialog.showOpenDialog((fileNames) => {
		if(fileNames === undefined){
			alert('No Files Selected');
		} else {
			readFile(fileNames[0]);
		}
	});
};

document.getElementById('updateButton').onclick = () => {
	dialog.showOpenDialog((fileNames) => {
		if(fileNames === undefined){
			alert('No Files Selected');
		} else {
			let content = document.getElementById('content').value;
			fs.writeFile(fileNames[0], content, (err) => {
				if(err){
					alert('An error occurred updating the file.');
					return;
				}
				alert('Ther file has been successfully updated');
			});
		}
	});
};


function readFile(filepath) {
	fs.readFile(filepath, 'utf-8', (err, data) => {
		if(err){
			alert('An error as occured reading the file.');
			return;
		}
		let textArea = document.getElementById('output');
		textArea.value = data;
	});
}